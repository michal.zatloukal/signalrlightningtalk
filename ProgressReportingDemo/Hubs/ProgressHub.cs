﻿using Microsoft.AspNetCore.SignalR;
using ProgressReportingDemo.Solving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProgressReportingDemo.Hubs
{
    public class ProgressHub : Hub
    {
        private readonly SolvingService _processingService;
        public ProgressHub(SolvingService processingService)
        {
            _processingService = processingService;
        }

        public async Task ReportProgress(double progress, string connectionId)
        {
            await Clients.Client(connectionId).SendAsync("ReceiveProgress", progress);
        }

        public Task SendFormula(string formula)
        {
            _processingService.AddItemToQueue(formula, Context.ConnectionId);
            return Task.CompletedTask;
        }
    }
}
