﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProgressReportingDemo.Reporting;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProgressReportingDemo.Solving
{
    public class SolvingService : BackgroundService
    {
        private readonly ILogger<SolvingService> _logger;
        private readonly BlockingCollection<(string formula, string connectionId)> _queue = new BlockingCollection<(string formula, string connectionId)>();
        private readonly ReportingService _reportingService;

        public SolvingService(ILogger<SolvingService> logger, ReportingService reportingService)
        {
            _logger = logger;
            _reportingService = reportingService;
        }

        public void AddItemToQueue(string formula, string connectionId)
        {
            _queue.Add((formula, connectionId));
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.Run(async () => await PeriodicallyProcessFormulas(stoppingToken));
        }

        private async Task PeriodicallyProcessFormulas(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                (string formula, string connectionId) item;
                bool itemPresent = _queue.TryTake(out item, 5000);

                if (!itemPresent)
                {
                    continue;
                }

                _logger.LogInformation("Processing formula: {formula}", item.formula);

                await Task.Delay(2000, stoppingToken);
                await _reportingService.ReportProgress(25, item.connectionId);

                await Task.Delay(2000, stoppingToken);
                await _reportingService.ReportProgress(50, item.connectionId);

                await Task.Delay(2000, stoppingToken);
                await _reportingService.ReportProgress(75, item.connectionId);

                await Task.Delay(2000, stoppingToken);
                _logger.LogInformation("Processed formula: {formula}", item.formula);
                await _reportingService.ReportResult("175.6g H2O", item.connectionId);
            }
        }
    }
}
