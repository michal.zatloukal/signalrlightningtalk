﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/progresshub").build();

//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;

connection.on("ReceiveProgress", function (progress) {
    var li = document.createElement("li");
    li.textContent = "Progress: " + progress + " %";
    document.getElementById("messagesList").appendChild(li);
});

connection.on("ReceiveResult", function (result) {
    var msg = result.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = "Result: " + msg;
    var li = document.createElement("li");
    li.textContent = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var formula = document.getElementById("formulaInput").value;
    connection.invoke("SendFormula", formula).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});