﻿using Microsoft.AspNetCore.SignalR;
using ProgressReportingDemo.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProgressReportingDemo.Reporting
{
    public class ReportingService
    {
        private readonly IHubContext<ProgressHub> _hubContext;
        public ReportingService(IHubContext<ProgressHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task ReportProgress(double progress, string connectionId)
        {
            await _hubContext.Clients.Client(connectionId).SendAsync("ReceiveProgress", progress);
        }

        public async Task ReportResult(string result, string connectionId)
        {
            await _hubContext.Clients.Client(connectionId).SendAsync("ReceiveResult", result);
        }
    }
}
